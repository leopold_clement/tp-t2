#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include "acquisitionManager.h"
#include "msg.h"
#include "iSensor.h"
#include "multitaskingAccumulator.h"
#include "iAcquisitionManager.h"
#include "debug.h"
#include "queue.h"

#define SEM_NAME_AQUISITION "SEM_Aquisition"
#define SEMAPHORE_INITIAL_VALUE_AQUISITION 1

// producer count storage
volatile unsigned int produceCount = 0;

pthread_t producers[4];

static void *produce(void *params);

/**
 * Semaphores and Mutex
 */
static sem_t *sem_aquisition_count;
static Pipe_t buffer_lecture;
/*
 * Creates the synchronization elements.
 * @return ERROR_SUCCESS if the init is ok, ERROR_INIT otherwise
 */
static unsigned int createSynchronizationObjects(void);

/*
 * Increments the produce count.
 */
static void incrementProducedCount(void);

static unsigned int createSynchronizationObjects(void)
{

	sem_unlink(SEM_NAME_AQUISITION);
	sem_aquisition_count = sem_open(SEM_NAME_AQUISITION, O_CREAT, 0644, SEMAPHORE_INITIAL_VALUE_AQUISITION);
	printf("[acquisitionManager]Semaphore created\n");
	pipe_init(&buffer_lecture);
	return ERROR_SUCCESS;
}

static void incrementProducedCount(void)
{
	sem_wait(sem_aquisition_count);
	produceCount++;
	sem_post(sem_aquisition_count);
}

unsigned int getProducedCount(void)
{
	unsigned int p = 0;
	sem_wait(sem_aquisition_count);
	p = produceCount;
	sem_post(sem_aquisition_count);
	return p;
}

MSG_BLOCK getMessage(void)
{
	return *read_pipe(&buffer_lecture);
}

// TODO create accessors to limit semaphore and mutex usage outside of this C module.

unsigned int acquisitionManagerInit(void)
{
	unsigned int i;
	unsigned int *p_i;
	printf("[acquisitionManager]Synchronization initialization in progress...\n");
	fflush(stdout);
	if (createSynchronizationObjects() == ERROR_INIT)
		return ERROR_INIT;

	printf("[acquisitionManager]Synchronization initialization done.\n");

	for (i = 0; i < PRODUCER_COUNT; i++)
	{
		p_i = &i;
		pthread_create(producers + i, NULL, produce, p_i);
	}

	return ERROR_SUCCESS;
}

void acquisitionManagerJoin(void)
{
	unsigned int i;
	for (i = 0; i < PRODUCER_COUNT; i++)
	{
		pthread_join(producers[i], NULL);
	}
	sem_destroy(sem_aquisition_count);
	// TODO destroy pipe
	printf("[acquisitionManager]Semaphore cleaned\n");
}

void *produce(void *params)
{
	unsigned int i = 0;
	unsigned int id = ((unsigned int *)params)[0];
	D(printf("[acquisitionManager]Producer created with id %d as %d\n", gettid(), id));

	MSG_BLOCK msg;
	while (i < PRODUCER_LOOP_LIMIT)
	{
		i++;
		sleep(PRODUCER_SLEEP_TIME + (rand() % 5));
		getInput(id, &msg);
		switch (messageCheck(&msg))
		{
		case 1:
			write_pipe(&buffer_lecture, &msg);
			incrementProducedCount();
			break;

		default:
			printf("[acquisitionManager - %d]Message invalide\n", id);
			break;
		}
	}
	printf("[acquisitionManager] %d termination\n", gettid());
	// TODO ??
	return;
}