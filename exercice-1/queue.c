//
// Created by theo on 16/11/22.
//
#include <semaphore.h>
#include <fcntl.h>
#include "msg.h"
#include "queue.h"

#define SEMAPHORE_INIT_VALUE_QUEUE 1
#define TAILLE_BUFFER 10
#define SEMAPHORE_INITIAL_VALUE_QUEUE 0

void write_pipe(Pipe_t *self, MSG_BLOCK *mBlock)
{
    sem_wait(self->W);
    sem_wait(self->Com1);
    self->i = self->tableau_libre[self->i_libre];
    self->i_libre = (self->i_libre + 1) % TAILLE_BUFFER;
    sem_post(self->Com1);

    self->buffer[self->i] = mBlock;

    sem_wait(self->Com2);
    self->tableau_occ[self->iocc] = self->i;
    self->iocc = (self->iocc + 1) % TAILLE_BUFFER;
    sem_post(self->Com2);
    sem_post(self->R);
}

MSG_BLOCK *read_pipe(Pipe_t *self)
{
    sem_wait(self->R);
    sem_wait(self->Com3);
    self->j = self->tableau_occ[self->jocc];
    self->jocc = (self->jocc + 1) % TAILLE_BUFFER;
    sem_post(self->Com3);

    MSG_BLOCK *v = self->buffer[self->j];

    sem_wait(self->Com4);
    self->tableau_libre[self->j_libre] = self->j;
    self->j_libre = (self->j_libre + 1) % TAILLE_BUFFER;
    sem_post(self->Com4);
    sem_post(self->W);

    return v;
}

void pipe_init(Pipe_t *self)
{
    sem_unlink("W");
    sem_unlink("R");
    sem_unlink("Com1");
    sem_unlink("Com2");
    sem_unlink("Com3");
    sem_unlink("Com4");

    self->W = sem_open("W", O_CREAT, 0644, TAILLE_BUFFER);
    self->R = sem_open("R", O_CREAT, 0644, SEMAPHORE_INITIAL_VALUE_QUEUE);
    self->Com1 = sem_open("Com1", O_CREAT, 0644, SEMAPHORE_INITIAL_VALUE_QUEUE);
    self->Com2 = sem_open("Com2", O_CREAT, 0644, SEMAPHORE_INITIAL_VALUE_QUEUE);
    self->Com3 = sem_open("Com3", O_CREAT, 0644, SEMAPHORE_INITIAL_VALUE_QUEUE);
    self->Com4 = sem_open("Com4", O_CREAT, 0644, SEMAPHORE_INITIAL_VALUE_QUEUE);

    sem_post(self->Com1);
    sem_post(self->Com2);
    sem_post(self->Com3);
    sem_post(self->Com4);
}

// pipe deconstuctor