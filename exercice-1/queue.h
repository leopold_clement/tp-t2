#ifndef QUEUE_H
#define QUEUE_H

#include <semaphore.h>
#include "msg.h"
#define SEMAPHORE_INIT_VALUE_QUEUE 1
#define TAILLE_BUFFER 10
#define SEMAPHORE_INITIAL_VALUE_QUEUE 0

typedef struct Pipe_t
{
    sem_t *Com1;
    sem_t *Com2;
    sem_t *Com3;
    sem_t *Com4;
    sem_t *W;
    sem_t *R;
    int i_libre;
    int j_libre;
    int i;
    int j;
    int iocc;
    int jocc;
    int tableau_libre[TAILLE_BUFFER];
    int tableau_occ[TAILLE_BUFFER];
    MSG_BLOCK *buffer[TAILLE_BUFFER];
} Pipe_t;

void write_pipe(Pipe_t *self, MSG_BLOCK *mBlock);

MSG_BLOCK *read_pipe(Pipe_t *self);

void pipe_init(Pipe_t *self);

#endif